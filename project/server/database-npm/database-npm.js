
'use strict';

const DatabaseCache = require('../core/database-cache');
const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const ChildProcess = require('child_process');
const Fs = require('fs');
const Path = require('path');


class DatabaseNpm {
  constructor(dbName, isReadOnly, releaseData) {
    this.dbName = dbName;
    this.isReadOnly = isReadOnly;
    this.releaseData = releaseData;
    this.databasePath = ActorPathGenerated.getDatabasesNpmFolder();
    this.databaseFile = `${this.databasePath}${Path.sep}node_modules${Path.sep}${this.releaseData.organization}${Path.sep}${this.dbName}${Path.sep}project${Path.sep}server`;
    this.npmInstall = `npm install --prefix ${this.databasePath} ${this.releaseData.organization}/${this.dbName}@latest --no-save --ignore-scripts --package-lock false`;
  }
  
  loadDatabase(cb) {
    ChildProcess.exec(this.npmInstall, (err, stdout, stderr) => {
      if(!err) {
        this._readDatabase((err, definition, data) => {
          cb(err, this.dbName, definition, data);
        });
      }
      else {
        cb(err, this.dbName);
      }
    });
  }
  
  fetchDatabase(cb) {
    this.loadDatabase(cb);
  }
  
  _readDatabase(cb) {
    let pendings = 2;
    let definitionJson = '';
    let dataJson = '';
    let hasError = false;
    Fs.readFile(`${this.databaseFile}${Path.sep}definition.json`, (err, data) => {
      if(!hasError) {
        if(err) {
          hasError = true;
          cb(err, null, null);
        }
        else {
          try {
            definitionJson = JSON.parse(data);
          }
          catch(err) {
            hasError = true;
            cb(err, null, null);
            return;
          }
          if(0 === --pendings) {
            cb(null, definitionJson, dataJson);
          }
        }
      }
    });
    Fs.readFile(`${this.databaseFile}${Path.sep}data.json`, (err, data) => {
      if(!hasError) {
        if(err) {
          hasError = true;
          cb(err, null, null);
        }
        else {
          try {
            dataJson = JSON.parse(data);
          }
          catch(err) {
            hasError = true;
            cb(err, null, null);
            return;
          }
          if(0 === --pendings) {
            cb(null, definitionJson, dataJson);
          }
        }
      }
    });
  }
}


module.exports = DatabaseNpm;
