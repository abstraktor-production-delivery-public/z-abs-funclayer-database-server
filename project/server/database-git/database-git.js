
'use strict';

const ActorPathGenerated = require('z-abs-corelayer-server/server/path/actor-path-generated');
const GitSimple = require('simple-git');
const Fs = require('fs');
const Path = require('path');


class DatabaseGit {
  constructor(dbName, isReadOnly, releaseData) {
    this.dbName = dbName;
    this.isReadOnly = isReadOnly;
    this.databasePath = `${ActorPathGenerated.getDatabasesGitFolder()}${Path.sep}${releaseData.releaseStep + (releaseData.organization ? Path.sep + releaseData.organization : '')}${Path.sep}${this.dbName}`;
    this.databaseFileDefinition = `${this.databasePath}${Path.sep}project${Path.sep}server${Path.sep}definition.json`;
    this.databaseFileData = `${this.databasePath}${Path.sep}project${Path.sep}server${Path.sep}data.json`;
  }
  
  loadDatabase(cb) {
    let pendings = 2;
    let configList = [];
    let configListError = null;
    let statError = null;
    GitSimple('.').listConfig((err, _configList) => {
      configList = _configList
      configListError = err;
      if(0 === --pendings) {
        this._loadDatabase(configListError, configList, statError, (err, definition, data) => {
          cb(err, this.dbName, definition, data);
        });
      }
    });                            
    Fs.stat(this.databasePath, (err, stat) => {
      statError = err;
      if(0 === --pendings) {
        this._loadDatabase(configListError, configList, statError, (err, definition, data) => {
          cb(err, this.dbName, definition, data);
        });
      }
    });
  }
  
  _loadDatabase(configListError, configList, statError, cb) {
    if(configListError) {
      ddb.error('Could not get git config.');
      cb(configListError);
      return;
    }
    let isHttp = false; 
    for(let i = configList.files.length - 1; i >= 0; --i) {
      const remoteOriginUrl = configList.values[configList.files[i]]['remote.origin.url'];
      if(remoteOriginUrl?.startsWith('https://')) {
        isHttp = true; 
        break;
      }
    }
    if(statError) {
      if('ENOENT' === statError.code) {
        Fs.mkdir(this.databasePath, {recursive: true}, (err) => {
          if(err) {
            cb(err);
            return;
          }
          else {
            this._cloneDatabase(isHttp, (err) => {
              if(err) {
                cb(err);
                return;
              }
              this._readDatabase(cb);
            });
          }
        }); 
      }
      else {
        cb(statError);
      }
    }
    else {
      this.fetchDatabase(cb);
    }
  }
  
  fetchDatabase(cb) {
    GitSimple(this.databasePath).fetch(['--no-tags'], (err, result) => {
      if(err) {
        cb(err);
        return;
      }
      if(0 !== result.updated.length || 0 !== result.deleted.length) {
        GitSimple(this.databasePath).reset(this.databasePath, 'hard', 'HEAD', (err, result) => {
          if(err) {
            cb(err);
            return;
          }
          GitSimple(this.databasePath).merge(this.databasePath, 'orig', '@{u}', ['--no-verify'], (err, result) => {
            if(err) {
              cb(err);
              return;
            }
            this._readDatabase(cb);
          });
        });
      }
      else {
        this._readDatabase(cb);
      }
    });
  }
  
  _readDatabase(cb) {
    let pendings = 2;
    let definitionJson = '';
    let dataJson = '';
    let hasError = false;
    Fs.readFile(this.databaseFileDefinition, (err, data) => {
      if(!hasError) {
        if(err) {
          hasError = true;
          cb(err, null, null);
        }
        else {
          try {
            definitionJson = JSON.parse(data);
          }
          catch(err) {
            hasError = true;
            cb(err, null, null);
            return;
          }
          if(0 === --pendings) {
            cb(null, definitionJson, dataJson);
          }
        }
      }
    });
    Fs.readFile(this.databaseFileData, (err, data) => {
      if(!hasError) {
        if(err) {
          hasError = true;
          cb(err, null, null);
        }
        else {
          try {
            dataJson = JSON.parse(data);
          }
          catch(err) {
            hasError = true;
            cb(err, null, null);
            return;
          }
          if(0 === --pendings) {
            cb(null, definitionJson, dataJson);
          }
        }
      }
    });
  }
  
  _cloneDatabase(isHttp, cb) {
    const databaseRepo = isHttp ? `https://gitlab.com/abstraktor-database/${this.dbName}.git` : `git@gitlab.com:abstraktor-database/${this.dbName}.git`;
    GitSimple(Path.resolve(`${this.databasePath}${Path.sep}..`), ['--no-tags', '--single-branch', '--depth=1']).clone(databaseRepo, (err, result) => {
      cb(err);
    });
  }
}


module.exports = DatabaseGit;
