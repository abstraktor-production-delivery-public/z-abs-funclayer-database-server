
'use strict';

const TableEncoder = require('./table-encoder');
const TablePage = require('./table-page');
const DatabaseConst = require('../core/database-const');


class Table {
  constructor(tableName, types, fileName) {
    this.tableName = tableName;
    this.types = types;
    this.baseFileName = fileName;
    this.nextRowIndex = 0;
    this.nextPageIndex = 0;
    this.meanSizeOfFullPages = 0;
    this.tablePages = [];
    this.pageSize = 8192;
    this.pageLock = false;
    this.pageLockQueue = [];
    TablePage.indexBuffer = Buffer.allocUnsafeSlow(TablePage.INDEX_SIZE);
    TablePage.dataBuffer = Buffer.allocUnsafeSlow(this.pageSize).fill(0);
    TableEncoder.size = this.pageSize;
  }
  
  createTable(cb) {
    const tablePage = this._getTablePageNext();
    tablePage.createFile((err) => {
      if(err) {
        cb(err);
      }
      else {
        this.tablePages[this.nextPageIndex].active(true, cb);
      }
    });
  }
  
  addTablePage(id) {
    const tablePage = new TablePage(id, this.tableName, this.nextRowIndex, this.pageSize, this.types, this.baseFileName);
    this.tablePages.push(tablePage);
    return tablePage;
  }
  
  parseTableFiles(fileDatas, table, cb) {
    let fileData = fileDatas.shift();
    let id = -1;
    this.nextPageIndex = -1;
    do { 
      ++this.nextPageIndex;
      const result = TablePage.parseFile(fileData.data, table.types, table.defaults, this.pageSize);
      if(0 !== result.rows.length) {
        Array.prototype.push.apply(table.rows, result.rows);
        const tablePage = this.addTablePage(++id);
        tablePage.stopRowIndex += result.rows.length;
        tablePage.freeSpace = result.freeSpace;
        tablePage.indexes = result.indexes;
        this.nextRowIndex += result.rows.length;
      }
      fileData = fileDatas.shift();
    } while(fileData);
    const lastPos = this.tablePages.length - 1;
    if(2 <= this.tablePages.length) {
      const lastTablePage = this.tablePages[lastPos];
      this.meanSizeOfFullPages = (this.nextRowIndex - (lastTablePage.stopRowIndex - lastTablePage.startRowIndex)) / (lastPos);
    }
    else {
      this.meanSizeOfFullPages = 0;
    }
    if(lastPos >= 0) {
      this.tablePages[lastPos].active(true, cb);
    }
    else {
      cb(null);
    }
  }
  
  insertRow(row, cb) {
    const tablePage = this._getTablePageNext();
    if(!tablePage.insertRow(row, (err) => {
      if(!err) {
        ++this.nextRowIndex;
      }
      cb(err);
    })) {
      this._createTablePage((err) => {
        if(err) {
          cb(err);
        }
        else {
          const currentTablePage = this.tablePages[this.nextPageIndex];
          this.meanSizeOfFullPages = this.meanSizeOfFullPages + ((currentTablePage.stopRowIndex - currentTablePage.startRowIndex) - this.meanSizeOfFullPages) / (this.tablePages.length - 1);
          this.tablePages[this.nextPageIndex].active(false, () => {
            ++this.nextPageIndex;
            this.tablePages[this.nextPageIndex].active(true, () => {
              this.insertRow(row, cb);
            });
          });
        }
      });
    }
  }
  
  updateRow(row, rowIndex, cb) {
    const possiblePageIndex = 0 === this.meanSizeOfFullPages ? 0 : Math.floor(rowIndex / this.meanSizeOfFullPages);
    const tablePage = this._getPageFromRow(possiblePageIndex, rowIndex);
    if(tablePage) {
      tablePage.updateRow(row, rowIndex, cb);
    }
    else {
      process.nextTick(cb, new Error('Update row not found:', rowIndex));
    }
  }
  
  _createTablePage(cb) {
    if(this.pageLock) {
      this.pageLockQueue.push(cb);
    }
    else {
      this.pageLock = true;
      const tablePage = new TablePage(this.tablePages.length, this.tableName, this.nextRowIndex, this.pageSize, this.types, this.baseFileName);
      this.tablePages.push(tablePage);
      tablePage.createFile((err) => {
        let queuedCb = this.pageLockQueue.shift();
        while(queuedCb) {
          queuedCb(err);
          queuedCb = this.pageLockQueue.shift();
        }
        this.pageLock = false;
        cb(err);
      });
    }
  }
  
  _getTablePageNext() {
    return this.tablePages[this.nextPageIndex];
  }
  
  _getPageFromRow(possiblePageIndex, rowIndex) {
    const possiblePage = this.tablePages[possiblePageIndex];
    if(rowIndex >= possiblePage.startRowIndex) {
      if(rowIndex < possiblePage.stopRowIndex) {
        return possiblePage;
      }
      else {
        if(this.tablePages.length < ++possiblePageIndex) {
          return this._getPageFromRow(possiblePageIndex);
        }
        else {
          return null;
        }
      }
    }
    else {
      if(0 >= --possiblePageIndex) {
        return this._getPageFromRow(possiblePageIndex);
      }
      else {
        return null;
      }
    }
  }
}


module.exports = Table;
