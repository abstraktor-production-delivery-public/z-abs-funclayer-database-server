
'use strict';

const TableDecoder = require('./table-decoder');
const TableEncoder = require('./table-encoder');
const DatabaseConst = require('../core/database-const');
const Fs = require('fs');
const Path = require('path');


class TablePage {
  static GROW_SIZE = 0;
  static INDEX_SIZE = 5;
  static indexBuffer = null;
  static dataBuffer = null;
  
  constructor(id, tableName, rowIndex, pageSize, types, baseFileName) {
    this.id = id;
    this.tableName = tableName;
    this.startRowIndex = rowIndex;
    this.stopRowIndex = rowIndex;
    this.indexes = [{start:-1, stop:-1, status:DatabaseConst.ROW_STATUS_NONE}];
    this.types = types;
    this.pageSize = pageSize;
    this.freeSpace = pageSize;
    this.dataFileName = `${baseFileName}-${id.toString().padStart(12, '0')}-data`;
    this.fd = -1;
  }
  
  active(isActive, cb) {
    if(isActive) {
      if(-1 === this.fd) {
        Fs.open(this.dataFileName, 'r+', (err, fd) => {
          if(err) {
            ddb.error(`Could not open filehandler: '${this.dataFileName}'`);
          }
          else {
            this.fd = fd;
          }
          cb(err);
        });
      }
      else {
        process.nextTick(cb);
      }
    }
    else {
      if(-1 === this.fd) {
        process.nextTick(cb);
      }
      else {
        Fs.close(this.fd, (err) => {
          if(err) {
            ddb.error(`Could not close filehandler: '${this.dataFileName}'`);
          }
          else {
            this.fd = -1;
          }
          cb(err);
        });
      }
    }
  }
  
  insertRow(row, cb) {
    const size = TableEncoder.calculateSize(row, this.types);
    if(this.freeSpace - size - TablePage.INDEX_SIZE >= TablePage.GROW_SIZE) {
      this.freeSpace -= (size + TablePage.INDEX_SIZE);
      const buffer = TableEncoder.encodeRow(row, this.types, size);
      const index = this.stopRowIndex - this.startRowIndex;
      const indexData = this.indexes[index];
      const start = indexData.stop + 1;
      const stop = start + size;
      const newIndexData = {start, stop, status:DatabaseConst.ROW_STATUS_EXISTS};
      this._write(buffer, newIndexData, this.indexes.length, (err) => {
        if(!err) {
          this.indexes[index] = newIndexData;
          this.indexes.push({start:-1, stop, status:DatabaseConst.ROW_STATUS_NONE});
          ++this.stopRowIndex;
        }
        cb(err);
      });
      return true;
    }
    else {
      return false;
    }
  }
  
  updateRow(row, rowIndex, cb) {
    const index = this.indexes[rowIndex - this.startRowIndex];
    console.log(index);
    const size = TableEncoder.calculateSize(row, this.types);
    if(size === index.stop -index.start) {
      console.log('SAME SIZE');
      const buffer = TableEncoder.encodeRow(row, this.types, size);
      this._write(buffer, index, this.indexes.length, cb);
    }
    else {
      console.log('!SAME SIZE!');
      process.nextTick(cb, new Error('Update row not the same size'));
    }
  }
  
  _writeClosed(buffer, indexData, index, cb) {
    Fs.open(this.dataFileName, 'r+', (errOpen, fd) => {
      if(errOpen) {
        cb(errOpen);
        return;
      }
      this._writeOpen(fd, buffer, indexData, index, (errWrite) => {
        Fs.close(fd, (errClose) => {
          cb(errWrite ? errWrite : errClose);
        });
      });
    });
  }
  
  _writeOpen(fd, buffer, indexData, index, cb) {
    Fs.write(fd, buffer, 0, indexData.stop - indexData.start, indexData.start, (err, bytes) => {
      if(err) {
        cb(err);
        return;
      }
      TablePage.indexBuffer.writeUInt16BE(indexData.start, 0);
      TablePage.indexBuffer.writeUInt16BE(indexData.stop, 2);
      TablePage.indexBuffer.writeUInt8(indexData.status, 4);
      Fs.write(fd, TablePage.indexBuffer, 0, TablePage.INDEX_SIZE, this.pageSize - index * TablePage.INDEX_SIZE, (err, bytes) => {
        cb(err);
      });
    });
  }
  
  _write(buffer, indexData, index, cb) {
    if(-1 !== this.fd) {
      this._writeOpen(this.fd, buffer, indexData, index, cb);
    }
    else {
      this._writeClosed(buffer, indexData, index, cb);
    }
  }
    
  createFile(cb) {
    Fs.writeFile(this.dataFileName, TablePage.dataBuffer, cb);
  }
  
  static parseFile(data, types, defaults, pageSize) {
    let freeSpace = pageSize;
    let index = 0;
    let pos = pageSize - TablePage.INDEX_SIZE;
    const indexes = [{start:-1, stop:-1, status:DatabaseConst.ROW_STATUS_NONE}];
    const rows = [];
    TableDecoder.decoder.buffer = data;
    while(freeSpace >= TablePage.GROW_SIZE) {
      const start = data.readUInt16BE(pos);
      const stop = data.readUInt16BE(pos + 2);
      const status = data.readUInt8(pos + 4);
      const indexData = indexes[index++];
      if(0 === stop) {
        break;
      }
      indexData.start = start;
      indexData.stop = stop;
      indexData.status = status;
      indexes.push({start:-1, stop, status:DatabaseConst.ROW_STATUS_NONE});
      const row = [];
      TableDecoder.decodeRow(row, types, defaults, start, stop);
      rows.push(row);
      freeSpace -= (stop - start + TablePage.INDEX_SIZE);
      pos -= TablePage.INDEX_SIZE;
    }
    TableDecoder.decoder.clear();
    return {
      indexes,
      rows,
      freeSpace
    };
  }
}


module.exports = TablePage;
