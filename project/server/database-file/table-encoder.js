
'use strict';

const DatabaseConst = require('../core/database-const');
const Encoder = require('z-abs-corelayer-server/server/communication/core-protocol/encoder');


class TableEncoder {
  static encoder = new Encoder(null);
  static size = 0;
  
  static calculateSize(row, types) {
    TableEncoder.encoder.calculate(true);
    TableEncoder._encodeRow(row, types);
    const size = TableEncoder.encoder.offset;
    TableEncoder.encoder.calculate(false);
    TableEncoder.encoder.clear();
    return size;
  }

  static encodeRow(row, types, size) {
    TableEncoder.encoder.createBuffer(size);
    TableEncoder._encodeRow(row, types);
    const buffer = TableEncoder.encoder.buffer;
    TableEncoder.encoder.clear();
    return buffer;
  }
  
  static _encodeRow(row, types) {
    for(let i = 0; i < types.length; ++i) {
      switch(types[i]) {
        case DatabaseConst.COLUMN_TYPE_GUID:
          TableEncoder.encoder.setGuid(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_INT8:
          TableEncoder.encoder.setInt8(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_INT16:
          TableEncoder.encoder.setInt16(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_INT32:
          TableEncoder.encoder.setInt32(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_INT64:
          TableEncoder.encoder.setBigInt64(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_UINT8:
          TableEncoder.encoder.setUint8(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_UINT16:
          TableEncoder.encoder.setUint16(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_UINT32:
          TableEncoder.encoder.setUint32(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_UINT64:
          TableEncoder.encoder.setBigUint64(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_FLOAT32:
          TableEncoder.encoder.setFloat32(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_FLOAT64:
          TableEncoder.encoder.setFloat64(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_STRING:
          TableEncoder.encoder.setString(row[i]);
          break;
        case DatabaseConst.COLUMN_TYPE_TIMESTAMP:
          TableEncoder.encoder.setBigInt64(row[i]);
          break;
        default:
  #BUILD_DEBUG_START
            ddb.warning('DB type: ', types[i], ' is not supported.');
  #BUILD_DEBUG_STOP
          break;
      };
    }
  }
}


module.exports = TableEncoder;
