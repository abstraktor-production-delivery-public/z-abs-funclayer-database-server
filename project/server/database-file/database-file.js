
'use strict';

const Table = require('./table');
const DatabaseConst = require('../core/database-const');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const GitSimple = require('simple-git');
const Fs = require('fs');
const Path = require('path');

// TODO: updates the whole file. Be smarter!
class DatabaseFile {
  constructor(dbName, isReadOnly, releaseData) {
    this.dbName = dbName;
    this.isReadOnly = isReadOnly;
    this.tables = new Map();
    if(ActorPath.releaseStepDelivery) {
      this.databaseFolder  = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}..${Path.sep}..${Path.sep}Database`);
    }
    else {
      this.databaseFolder  = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Database`);
    }
    this.databasePath = `${this.databaseFolder}${Path.sep}${releaseData.releaseStep + (releaseData.organization ? Path.sep + releaseData.organization : '')}${Path.sep}${this.dbName}`;
    this.databaseFileDefinition = `${this.databasePath}${Path.sep}project${Path.sep}server${Path.sep}definition.json`;
    this.databaseFileData = `${this.databasePath}${Path.sep}project${Path.sep}server${Path.sep}data.json`;
  }
  
  loadDatabase(cb) {
    let pendings = 2;
    let configList = [];
    let configListError = null;
    let statError = null;
    GitSimple('.').listConfig((err, configList) => {
      configListError = err;
      if(0 === --pendings) {
        this._loadDatabase(configListError, configList, statError, (err, definition, data) => {
          cb(err, this.dbName, definition, data);
        });
      }
    });                            
    Fs.stat(this.databasePath, (err, stat) => {
      statError = err;
      if(0 === --pendings) {
        this._loadDatabase(configListError, configList, statError, (err, definition, data) => {
          cb(err, this.dbName, definition, data);
        });
      }
    });
  }
  
  insertRow(tableName, row, cb) {
    const table = this.tables.get(tableName);
    table.insertRow(row, cb);
  }
  
  updateRow(tableName, row, rowIndex, cb) {
    const table = this.tables.get(tableName);
    table.updateRow(row, rowIndex, cb);
  }
  
  _loadDatabase(configListError, configList, statError, cb) {
    if(configListError) {
      ddb.error('Could not get git config.');
      cb(configListError);
      return;
    }
    let isHttp = false; 
    for(let i = configList.files.length - 1; i >= 0; --i) {
      const remoteOriginUrl = configList.values[configList.files[i]]['remote.origin.url'];
      if(remoteOriginUrl?.startsWith('https://')) {
        isHttp = true; 
        break;
      }
    }
    if(statError) {
      if('ENOENT' === statError.code) {
        Fs.mkdir(this.databasePath, {recursive: true}, (err) => {
          if(err) {
            cb(err);
            return;
          }
          else {
            this._cloneDatabase(isHttp, (err) => {
              if(err) {
                cb(err);
                return;
              }
              this._readDatabaseDefinition((err, definition) => {
                if(err) {
                  cb(err);
                  return;
                }
                this._createaDatabaseFiles(definition, cb);
              });
            });
          }
        }); 
      }
      else {
        cb(statError);
      }
    }
    else {
      this._readDatabase(cb);
    }
  }
  
  _readDatabaseDefinition(cb) {
    Fs.readFile(this.databaseFileDefinition, (err, data) => {
      if(err) {
        cb(err, null, null);
      }
      else {
        try {
          const definitionJson = JSON.parse(data);
          cb(null, definitionJson);
        }
        catch(err) {
          cb(err, null);
          return;
        }
      }
    });
  }
  
  _parseTableData(tableData, datas, cb) {
    const tablePath = `${this.databasePath}${Path.sep}${tableData.name}`;
    const fileName = `${tablePath}${Path.sep}${tableData.name}`;
    const table = new Table(tableData.name, tableData.types, fileName);
    this.tables.set(table.tableName, table);
    table.parseTableFiles(datas, tableData, cb);
  }
  
  _readTableData(tableData, cb) {
    const tablePath = `${this.databasePath}${Path.sep}${tableData.name}`;
    Fs.readdir(tablePath, (err, files) => {
      if(err) {
        cb(err);
      }
      else {
        let pendings = files.length;
        let datas = [];
        files.forEach((file) => {
          Fs.readFile(`${tablePath}${Path.sep}${file}`, (err, data) => {
            if(err) {
              cb(err);
            }
            else {
              datas.push({file, data});
              if(0 === --pendings) {
                datas.sort((a, b) => {
                  return a.file.localeCompare(b.file);
                });
                this._parseTableData(tableData, datas, cb);
              }
            }
          });
        });
      }
    });
  }
  
  _readDatabase(cb) {
    this._readDatabaseDefinition((err, definition) => {
      this._createaDatabaseData(definition, (err, definition, data) => {
        if(err) {
          cb(err);
        }
        else {
          let pendings = data.tables.length;
          data.tables.forEach((tableData) => {
            this._readTableData(tableData, (err) => {
              if(0 === --pendings) {
                cb(null, definition, data);
              }
            });
          });
        }
      });
    });
  }
  
  _cloneDatabase(isHttp, cb) {
    const databaseRepo = isHttp ? `https://gitlab.com/abstraktor-database/${this.dbName}.git` : `git@gitlab.com:abstraktor-database/${this.dbName}.git`;
    GitSimple(Path.resolve(`${this.databasePath}${Path.sep}..`), ['--no-tags', '--single-branch', '--depth=1']).clone(databaseRepo, (err, result) => {
      cb(err);
    });
  }
  
  _creaateDatabaseFile(fileName, cb) {
    Fs.writeFile(fileName, '', (err) => {
      cb(err);
    });
  }
  
  _handleDatabaseFiles(pendings, cb, err) {
    if(err) {
      pendings.errors.push(err);
      if(cb) {
        pendings.p -= 2;
      }
      else {
        --pendings.p;
      }
      if(0 === --pendings.p) {
        cb(new Error('Create Database Error.'));
      }
    }
    else {
      if(cb) {
        --pendings.p;
        cb();
      }
      else if(0 === --pendings.p) {
        if(0 !== pendings.errors.length) {
          pendings.cb(pendings.errors[0]);
        }
        else {
          pendings.cb();
        }
      }
    }
  }
  
  _createaDatabaseFiles(definition, cb) {
    let pendings = {
      p: definition.tables.length * 2,
      errors: [],
      cb: (err) => {
        if(err) {
          cb(new Error('Create Database Error.'));
        }
        else {
          this._createaDatabaseData(definition, cb);
        }
      }
    };
    definition.tables.forEach((definitionTable) => {
      const tablePath = `${this.databasePath}${Path.sep}${definitionTable.name}`;
      const fileName = `${tablePath}${Path.sep}${definitionTable.name}`;
      const types = DatabaseFile.getTypes(definitionTable.types);
      const table = new Table(definitionTable.name, types, fileName);
      table.addTablePage(0);
      this.tables.set(table.tableName, table);
      Fs.mkdir(tablePath, {recursive: true}, this._handleDatabaseFiles.bind(this, pendings, () => {
        table.createTable(this._handleDatabaseFiles.bind(this, pendings, null));
      }));
    });
  }
  
  _createaDatabaseData(definition, cb) {
    const data = {
      tables: []
    };
    definition.tables.forEach((definitionTable) => {
      const types = DatabaseFile.getTypes(definitionTable.types);
      const defaults = DatabaseFile.getDefaults(definitionTable.defaults, types);
      data.tables.push({
        name: definitionTable.name,
        types,
        defaults,
        rows: []
      });
    });
    cb(null, definition, data);
  }
  
  static getTypes(definedTypes) {
    if(!definedTypes) {
      return [];
    }
    const typeIds = [];
    definedTypes.forEach((definedType) => {
      typeIds.push(DatabaseConst.getType(definedType));
    });
    return typeIds;
  }
  
  static getDefaults(definedDefaults, types) {
    if(!definedDefaults) {
      return [];
    }
    if(0 === types.length) {
      return [];
    }
    const defaults = [];
    const lengthDiff = types.length - definedDefaults.length;
    for(let i = 0; i < definedDefaults.length; ++i) {
      const type = types[i + lengthDiff];
      if(DatabaseConst.COLUMN_TYPE_INT64 === type || DatabaseConst.COLUMN_TYPE_UINT64 === type || DatabaseConst.COLUMN_TYPE_TIMESTAMP) {
        defaults.push(BigInt(definedDefaults[i]));
      }
      else {
        defaults.push(definedDefaults[i]);
      }
    }
    return defaults;
  }
}


module.exports = DatabaseFile;
