
'use strict';

const DatabaseConst = require('../core/database-const');
const Decoder = require('z-abs-corelayer-server/server/communication/core-protocol/decoder');


class TableDecoder {
  static decoder = new Decoder();
  
  static decodeRow(row, types, defaults, start, stop) {
    TableDecoder.decoder.offset = start;
    for(let i = 0; i < types.length; ++i) {
      if(start <= stop) {
        switch(types[i]) {
          case DatabaseConst.COLUMN_TYPE_GUID:
            row.push(TableDecoder.decoder.getGuid());
            break;
          case DatabaseConst.COLUMN_TYPE_INT8:
            row.push(TableDecoder.decoder.getInt8());
            break;
          case DatabaseConst.COLUMN_TYPE_INT16:
            row.push(TableDecoder.decoder.getInt16());
            break;
          case DatabaseConst.COLUMN_TYPE_INT32:
            row.push(TableDecoder.decoder.getInt32());
            break;
          case DatabaseConst.COLUMN_TYPE_INT64:
            row.push(TableDecoder.decoder.getBigInt64());
            break;
          case DatabaseConst.COLUMN_TYPE_UINT8:
            row.push(TableDecoder.decoder.getUint8());
            break;
          case DatabaseConst.COLUMN_TYPE_UINT16:
            row.push(TableDecoder.decoder.getUint16());
            break;
          case DatabaseConst.COLUMN_TYPE_UINT32:
            row.push(TableDecoder.decoder.getUint32());
            break;
          case DatabaseConst.COLUMN_TYPE_UINT64:
            row.push(TableDecoder.decoder.getBigUint64());
            break;
          case DatabaseConst.COLUMN_TYPE_FLOAT32:
            row.push(TableDecoder.decoder.getFloat32());
            break;
          case DatabaseConst.COLUMN_TYPE_FLOAT64:
            row.push(TableDecoder.decoder.getFloat64());
            break;
          case DatabaseConst.COLUMN_TYPE_STRING:
            row.push(TableDecoder.decoder.getString());
            break;
          case DatabaseConst.COLUMN_TYPE_TIMESTAMP:
            row.push(TableDecoder.decoder.getBigInt64());
            break;
          default:
    #BUILD_DEBUG_START
              ddb.warning('DB type: ', types[i], ' is not supported.');
    #BUILD_DEBUG_STOP
            break;
        };
      }
    }
    if(row.length !== types.length) {
      let j = 0;
      const length = types.length;
      for(let i = row.length; i < length; ++i, ++j) {
        row.push(defaults[j]);
      }
    }
  }
}
 

module.exports = TableDecoder;
