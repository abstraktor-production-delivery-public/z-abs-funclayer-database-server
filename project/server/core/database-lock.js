
'use strict';


class DatabaseLock {
  static _id = 0n;
  
  constructor(databaseName) {
    this.databaseName = databaseName;
    this.locked = false;
    this.lockQueue = [];
    this.readLocks = 0;
    this.readLockQueue = [];
    this.isWriteLock = false;
    this.writeLockQueue = [];
  }
  
  lock(cb) {
    if(!this.locked) {
      this.locked = true;
      process.nextTick(cb);
    }
    else {
      this.lockQueue.push(cb);
    }
    this._verbose();
  }
  
  unlock() {
    if(0 === this.lockQueue.length) {
      this.locked = false;
    }
    else {
      const cb = this.lockQueue.shift();
      process.nextTick(cb);
    }
    this._verbose();
  }
  
  _verbose() {
    //console.log('isWriteLock:', this.isWriteLock, 'writeLockQueue:', this.writeLockQueue.length, 'readLocks:', this.readLocks, 'readLockQueue:', this.readLockQueue.length, 'locked:', this.locked, 'lockQueue:', this.lockQueue.length);
  }
  
  readLock(cb) {
    if(!this.isWriteLock && 0 === this.writeLockQueue.length) {
      ++this.readLocks;
      process.nextTick(cb);
    }
    else {
      this.readLockQueue.push(cb);
    }
    this._verbose();
  }
  
  readUnlock() {
    if(0 === --this.readLocks) {
      if(0 !== this.writeLockQueue.length) {
        this.isWriteLock = true;
        const cb = this.writeLockQueue.shift();
        process.nextTick(cb);
      }
    }
    this._verbose();
  }
  
  writeLock(cb) {
    if(!this.isWriteLock && 0 === this.readLocks) {
      this.isWriteLock = true;
      process.nextTick(cb);
    }
    else {
      this.writeLockQueue.push(cb);
    }
    this._verbose();
  }
  
  writeUnlock(cb) {
    if(0 !== this.writeLockQueue.length) {
      const cb = this.writeLockQueue.shift();
      process.nextTick(cb);
    }
    else {
      this.isWriteLock = false;
      if(0 !== this.readLockQueue.length) {
        while(0 !== this.readLockQueue.length) {
          ++this.readLocks;
          const cb = this.readLockQueue.shift();
          process.nextTick(cb); 
        }
      }
    }
    this._verbose();
  }
}


module.exports = DatabaseLock;
