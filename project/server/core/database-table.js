
'use strict';

const DatabaseConst = require('./database-const');


class DatabaseTable {
  constructor(definition, data, database) {
    this.indexes = new Map();
    this.rows = [];
    this.database = database;
    this.name = definition.name;
    this.columns = definition.columns;
    this.columnNames = this._getColumnNames();
    this.types = DatabaseTable.getTypes(definition.types);
    this.defaults = DatabaseTable.getDefaults(definition.defaults, this.types);
    this.definedIndexes = definition.indexes;
    this.pkLength = this._getPkLength(definition.pk);
    this.pkName = this._getPkName();
    this.identity = !!definition.identity;
    this.seed = this._getSeed(!!definition.seed ? definition.seed : 0);
    if(0 !== this.pkLength) {
      const indexes = [];
      for(let i = 0; i < this.pkLength; ++i) {
        indexes.push(i);
      }
      this.definedIndexes.unshift({
        name: this.pkName,
        indexes
      });
    }
    const indexes = definition.indexes;
    indexes.forEach((index) => {
      const indexValues = new Map();
      this.indexes.set(index.name, indexValues);
    });
    if(0 !== data.rows.length) {
      data.rows.forEach((row) => {
        const newRow = this._insertRowDefaults(row);
        if(newRow) {
          this._insertRow(newRow);
        }
      });
      if(this.identity && 1 === this.pkLength) {
        const type = this.types[0];
        if(DatabaseConst.COLUMN_TYPE_INT64 === type || DatabaseConst.COLUMN_TYPE_UINT64 === type || DatabaseConst.COLUMN_TYPE_INT32 === type || DatabaseConst.COLUMN_TYPE_UINT32 === type || DatabaseConst.COLUMN_TYPE_INT16 === type || DatabaseConst.COLUMN_TYPE_UINT16 === type || DatabaseConst.COLUMN_TYPE_INT8 === type || DatabaseConst.COLUMN_TYPE_UINT8 === type) {
          this.seed = this.rows[this.rows.length - 1][0];
        }
      }
    }
  }
  
  getRow(indexName, indexValue, cb) {
    const indexes = this.indexes.get(indexName);
    if(!indexes) {
      ddb.error('There is no index name ' + indexName + '.');
      return cb(null, null);
    }
    const index = indexes.get(indexValue);
    if(undefined === index) {
      return cb(null, null);
    }
    else {
      return cb(null, this.rows[index]);
    }
  }
  
  getRows(columnNames, columnValues, cbAccept, cb) {
    const columnIndexes = columnNames.map((columnName) => {
      for(let i = 0; i < this.columns.length; ++i) {
        if(columnName === this.columns[i]) {
          return i;
        }
      }
    });
    const results = [];
    this.rows.forEach((row) => {
      const match = columnIndexes.every((columnIndex, index) => {
        return row[columnIndex] === columnValues[index];
      });
      if(match) {
        if(cbAccept(row)) {
          results.push(row);
        }
      }
    });
    return cb(null, results);
  }
  
  insertRow(values, cb) {
    //const startTime = process.hrtime.bigint();
    if(this.identity && 1 === this.pkLength) {
      const type = this.types[0];
      if(DatabaseConst.COLUMN_TYPE_INT64 === type || DatabaseConst.COLUMN_TYPE_UINT64 === type || DatabaseConst.COLUMN_TYPE_INT32 === type || DatabaseConst.COLUMN_TYPE_UINT32 === type || DatabaseConst.COLUMN_TYPE_INT16 === type || DatabaseConst.COLUMN_TYPE_UINT16 === type || DatabaseConst.COLUMN_TYPE_INT8 === type || DatabaseConst.COLUMN_TYPE_UINT8 === type) {
        values.unshift(++this.seed);
      }
    }
    const row = this._insertRowDefaults(values);
    if(row) {
      this.database.insertRow(this.name, row, (err) => {
        if(!err) {
          this._insertRow(row);
        }
        //const stopTime = process.hrtime.bigint();
        //console.log('insertRow; nano seconds:', stopTime - startTime);
        cb(err);
      });
    }
    else {
      process.nextTick(cb, new Error('Data dropped, defaults lenght is not correct'));
    }
  }
  
  updateRow(pkValue, columnNames, columnValues, cb) {
    //const startTime = process.hrtime.bigint();
    const indexes = this.indexes.get(this.pkName);
    const rowIndex = indexes.get(pkValue);
    const row = this.rows[rowIndex];
    if(row) {
      for(let i = 0; i < columnNames.length; ++i) {
        const columnId = this.columnNames.get(columnNames[i]);
        row[columnId] = columnValues[i];
      }
      this.database.updateRow(this.name, row, rowIndex, (err) => {
        //const stopTime = process.hrtime.bigint();
        //console.log('updateRow; nano seconds:', stopTime - startTime);
        cb(err);
      });
    }
    else {
      process.nextTick(cb, new Error('Row not found.'));
    }
  }
  
  _insertRowDefaults(row) {
    if(this.columns.length === row.length) {
      return row;
    }
    else if(this.columns.length + this.defaults.length >= row.length && this.defaults.length >= row.length - this.columns.length) {
      let j = 0;
      const length = this.columns.length;
      for(let i = row.length; i < length; ++i, ++j) {
        row.push(this.defaults[j]);
      }
      return row;
    }
    else {
      ddb.error('Data dropped, defaults lenght is not correct');
      return null;
    }
  }
  
  _insertRow(row) {
    this.rows.push(row);
    this.definedIndexes.forEach((definedIndex) => {
      if(2 <= definedIndex.indexes.length) {
        const indexColumns = [];
        definedIndex.indexes.forEach((index) => {
          indexColumns.push(row[index]);
        });
        const indexValue = indexColumns.join('_');
        const index = this.indexes.get(definedIndex.name);
        #BUILD_DEBUG_START
        if(index.has(indexValue)) {
          ddb.warning('Index must be unique:', indexValue);
        }  
        #BUILD_DEBUG_STOP
  	    index.set(indexValue, this.rows.length - 1);
      }
      else {
        const indexValue = row[0];
        const index = this.indexes.get(definedIndex.name);
        #BUILD_DEBUG_START
        if(index.has(indexValue)) {
          ddb.warning('Index must be unique:', indexValue);
        }  
        #BUILD_DEBUG_STOP
  	    index.set(indexValue, this.rows.length - 1);
      }
	  });
  }
  
  static getTypes(definedTypes) {
    if(!definedTypes) {
      return [];
    }
    const typeIds = [];
    definedTypes.forEach((definedType) => {
      typeIds.push(DatabaseConst.getType(definedType));
    });
    return typeIds;
  }
  
  static getDefaults(definedDefaults, types) {
    if(!definedDefaults) {
      return [];
    }
    if(0 === types.length) {
      return [];
    }
    const defaults = [];
    const lengthDiff = types.length - definedDefaults.length;
    for(let i = 0; i < definedDefaults.length; ++i) {
      const type = types[i + lengthDiff];
      if(DatabaseConst.COLUMN_TYPE_INT64 === type || DatabaseConst.COLUMN_TYPE_UINT64 === type || DatabaseConst.COLUMN_TYPE_TIMESTAMP) {
        defaults.push(BigInt(definedDefaults[i]));
      }
      else {
        defaults.push(definedDefaults[i]);
      }
    }
    return defaults;
  }
  
  _getPkLength(pk) {
    if('number' === typeof pk) {
      return pk;
    }
    else {
      return 0;
    }
  }
  
  _getPkName() {
    if(1 === this.pkLength) {
      return this.columns[0];
    }
    else if(0 === this.pkLength) {
      return '';
    }
    else {
      const columns = [];
      for(let i = 0; i < this.pkLength; ++i) {
        columns.push(this.columns[i]);
      }
      return columns.join('_');
    }
  }
  
  _getSeed(seed) {
    if('number' === typeof seed && 1 === this.pkLength) {
      const type = this.types[0];
      if(DatabaseConst.COLUMN_TYPE_INT64 === type || DatabaseConst.COLUMN_TYPE_UINT64 === type) {
        return BigInt(seed);
      }
      else if(DatabaseConst.COLUMN_TYPE_INT32 === type || DatabaseConst.COLUMN_TYPE_UINT32 === type || DatabaseConst.COLUMN_TYPE_INT16 === type || DatabaseConst.COLUMN_TYPE_UINT16 === type || DatabaseConst.COLUMN_TYPE_INT8 === type || DatabaseConst.COLUMN_TYPE_UINT8 === type) {
        return seed;
      }
    }
    else {
      return 0;
    }
  }
  
  _getColumnNames() {
    const columnNames = new Map();
    for(let i = 0; i < this.columns.length; ++i) {
      columnNames.set(this.columns[i], i);
    }
    return columnNames;
  }
}


module.exports = DatabaseTable;
