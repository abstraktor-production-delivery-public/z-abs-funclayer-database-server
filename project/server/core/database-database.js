
'use strict';

const DatabaseConst = require('../core/database-const');
const DatabaseFile = require('../database-file/database-file');
const DatabaseGit = require('../database-git/database-git');
const DatabaseNpm = require('../database-npm/database-npm');
const DatabaseCache = require('./database-cache');
const Fs = require('fs');
const Path = require('path');


class DatabaseDatabase {
  static load(dbName, isReadOnly, dbType, releaseData, cb) {
    DatabaseCache.setLock(dbName);
    let database = null;
    const handleResponse = (err, dbName, definition, data) => {
      if(!err) {
        DatabaseCache.writeLock(dbName, () => {
          DatabaseCache.setLoaded(dbName, database, definition, data);
          DatabaseCache.writeUnlock(dbName);
          cb();
        });
      }
      else {
        ddb.error('Failed to load database path: \'' + dbName + '\'.', err);
        cb(err);
      }
    };
    if(DatabaseConst.TYPE_FILE === dbType) {
      database = new DatabaseFile(dbName, isReadOnly, releaseData);
    }
    else if(DatabaseConst.TYPE_GIT === dbType) {
      database = new DatabaseGit(dbName, isReadOnly, releaseData);
    }
    else if(DatabaseConst.TYPE_NPM === dbType) {
      database = new DatabaseNpm(dbName, isReadOnly, releaseData);
    }
    DatabaseCache.lockDatabase(dbName, () => {
      database.loadDatabase(handleResponse);
      DatabaseCache.unlockDatabase(dbName);
    });
  }
  
  static read(dbName, cb) {
    DatabaseCache.read(dbName, (err, definition, data) => {
      cb(dbName, definition, data);
    });
  }
  
  static getRow(dbName, tableName, pkName, pkValues, cb) {
    if(1 === pkValues.length) {
      if('object' === typeof(pkValues[0])) {
        ddb.warning(ddb.red('object value NOT IMPLEMENTED.'));
        cb(null, null);
      }
    }
    else {
      DatabaseCache.readLock(dbName, () => {
        const table = DatabaseCache.getTable(dbName, tableName, (err, table) => {
          if(err) {
            DatabaseCache.readUnlock(dbName);
            cb(err);
            return;
          }
          const key = pkValues.join('_');
          table.getRow(pkName, key, (err, row) => {
            DatabaseCache.readUnlock(dbName);
            cb(err, row);
          });
        });
      });
    }
  }
  
  static getRows(dbName, tableName, columnNames, columnValues, cbAccept, cb) {
    DatabaseCache.readLock(dbName, () => {
      const table = DatabaseCache.getTable(dbName, tableName, (err, table) => {
        if(err) {
          DatabaseCache.readUnlock(dbName);
          cb(err);
          return;
        }
        table.getRows(columnNames, columnValues, cbAccept, (err, rows) => {
          DatabaseCache.readUnlock(dbName);
          cb(err, rows);
        });
      });
    });
  }
  
  static insertRow(dbName, tableName, values, cb) {
    DatabaseCache.writeLock(dbName, () => {
      const table = DatabaseCache.getTable(dbName, tableName, (err, table) => {
        if(err) {
          DatabaseCache.writeUnlock(dbName);
          cb(err);
          return;
        }
        table.insertRow(values, (err, row) => {
          DatabaseCache.writeUnlock(dbName);
          cb(err, values);
        });
      });
    });
  }
  
  static updateRow(dbName, tableName, pkValue, columnNames, columnValues, cb) {
    DatabaseCache.writeLock(dbName, () => {
      const table = DatabaseCache.getTable(dbName, tableName, (err, table) => {
        if(err) {
          DatabaseCache.writeUnlock(dbName);
          cb(err);
          return;
        }
        table.updateRow(pkValue, columnNames, columnValues, (err, row) => {
          DatabaseCache.writeUnlock(dbName);
          cb(err);
        });
      });
    });
  }
}


module.exports = DatabaseDatabase;
