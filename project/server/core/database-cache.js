
'use strict';

const DatabaseLock = require('./database-lock');
const DatabaseTable = require('./database-table');


class DatabaseCache {
  constructor() {
    this.locks = new Map();
    this.databases = new Map();
    this.loaded = false;
    DatabaseCache._.instance = this;
  }
  
  static getInstance() {
    if(!DatabaseCache._.instance) {
      DatabaseCache._.instance = new DatabaseCache();
    }
    return DatabaseCache._.instance;
  }
  
  static setLock(dbName) {
    const instance = DatabaseCache.getInstance();
    if(!instance.locks.has(dbName)) {
      const databaseLock = new DatabaseLock(dbName);
      instance.locks.set(dbName, databaseLock);
    }
  }
  
  static setLoaded(dbName, database, definition, data) {
    const instance = DatabaseCache.getInstance();
    const databaseInstance = {
      name: dbName,
      database: database,
      fetchQueue: [],
      tables: new Map()
    };
    // TODO: Handle merge
    instance.databases.set(dbName, databaseInstance);
    for(let i = 0; i < definition.tables.length; ++i) {
      const tableDefinition = definition.tables[i];
      const tableData = data.tables[i];
      const dbTable = new DatabaseTable(tableDefinition, tableData, databaseInstance.database);
      databaseInstance.tables.set(tableDefinition.name, dbTable);
    }
  }
  
  static readLock(dbName, cb) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.readLock(cb);
  }
  
  static readUnlock(dbName) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.readUnlock();
  }
  
  static writeLock(dbName, cb) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.writeLock(cb);
  }
  
  static writeUnlock(dbName) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.writeUnlock();
  }
  
  static lockDatabase(dbName, cb) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.lock(cb);
  }
  
  static unlockDatabase(dbName) {
    const instance = DatabaseCache.getInstance();
    const databaseLock = instance.locks.get(dbName);
    databaseLock.unlock();
  }
    
  static read(dbName, cb) {
    const instance = DatabaseCache.getInstance();
    const databaseData = instance.databases.get(dbName);
    if(!databaseData) {
      cb(new Error(`Database: ${dbName} not found.`));
      return;
    }
    databaseData.fetchQueue.push(cb);
    if(1 === databaseData.fetchQueue.length) {
      DatabaseCache.lockDatabase(dbName, () => {
        databaseData.database.fetchDatabase((err, definition, data) => {
          DatabaseCache.unlockDatabase(dbName);
          let queuedCb;
          while(queuedCb = databaseData.fetchQueue.shift()) {
            process.nextTick(cb, err, definition, data);
          }
        });
      });
    }
  }
  
  static getTable(dbName, name, cb) {
    const instance = DatabaseCache.getInstance();
    const databaseData = instance.databases.get(dbName);
    if(!databaseData) {
      cb(new Error(`Database: ${dbName} not found.`));
      return;
    }
    const table = databaseData.tables.get(name);
    if(!table) {
      cb(new Error(`Table: ${name} not found in Database: ${dbName}.`));
      return;
    }
    cb(null, table);
  }
}


DatabaseCache._ =  {
  instance: null
};


module.exports = DatabaseCache;
