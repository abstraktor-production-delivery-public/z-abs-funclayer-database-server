
'use strict';


class DatabaseConst {
  static TYPE_NPM = 0;
  static TYPE_GIT = 1;
  static TYPE_FILE = 2;
  
  static STATUS_ACTIVE = 0;
  static STATUS_DELETED = 1;
  static STATUS_SUSPENDED = 2;
  
  static ROW_STATUS_NONE = 0;
  static ROW_STATUS_EXISTS = 1;
  static ROW_STATUS_DELETED = 2;
  
  static COLUMN_TYPE_UNKNOWN = 0;
  static COLUMN_TYPE_GUID = 1;
  static COLUMN_TYPE_INT8 = 2;
  static COLUMN_TYPE_INT16 = 3;
  static COLUMN_TYPE_INT32 = 4;
  static COLUMN_TYPE_INT64 = 5;
  static COLUMN_TYPE_UINT8 = 6;
  static COLUMN_TYPE_UINT16 = 7;
  static COLUMN_TYPE_UINT32 = 8;
  static COLUMN_TYPE_UINT64 = 9;
  static COLUMN_TYPE_FLOAT32 = 10;
  static COLUMN_TYPE_FLOAT64 = 11;
  static COLUMN_TYPE_STRING = 12;
  static COLUMN_TYPE_TIMESTAMP = 13;
  
  static getType(type) {
    switch(type) {
      case 'Guid':
        return DatabaseConst.COLUMN_TYPE_GUID;
      case 'Int8':
        return DatabaseConst.COLUMN_TYPE_INT8;
      case 'Int16':
        return DatabaseConst.COLUMN_TYPE_INT16;
      case 'Int32':
        return DatabaseConst.COLUMN_TYPE_INT32;
      case 'Int64':
        return DatabaseConst.COLUMN_TYPE_INT64;
      case 'UInt8':
        return DatabaseConst.COLUMN_TYPE_UINT8;
      case 'UInt16':
        return DatabaseConst.COLUMN_TYPE_UINT16;
      case 'UInt32':
        return DatabaseConst.COLUMN_TYPE_UINT32;
      case 'UInt64':
        return DatabaseConst.COLUMN_TYPE_UINT64;
      case 'Float32':
        return DatabaseConst.COLUMN_TYPE_FLOAT32;
      case 'Float64':
        return DatabaseConst.COLUMN_TYPE_FLOAT64;
      case 'String':
        return DatabaseConst.COLUMN_TYPE_STRING;
      case 'Timestamp':
        return DatabaseConst.COLUMN_TYPE_TIMESTAMP;
      default:
#BUILD_DEBUG_START
        ddb.warning('DB type: ', type, ' is not supported.');
#BUILD_DEBUG_STOP
        return DatabaseConst.COLUMN_TYPE_UNKNOWN;
    };
  }
}


module.exports = DatabaseConst;
